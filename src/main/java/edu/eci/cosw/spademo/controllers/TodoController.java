package edu.eci.cosw.spademo.controllers;

import edu.eci.cosw.spademo.models.Todo;
import edu.eci.cosw.spademo.services.TodoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * Created by amoto on 2/5/17.
 */
@RestController
@RequestMapping("/todo")
public class TodoController {
    @Autowired
    public TodoServices ts;

    @RequestMapping(method = RequestMethod.GET)
    public ArrayList<Todo> todos(){
        return ts.getTodos();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addTodo(@RequestBody Todo todo){
        ts.addTodo(todo);
    }
}
