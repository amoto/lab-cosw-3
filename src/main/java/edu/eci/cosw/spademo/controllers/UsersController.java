package edu.eci.cosw.spademo.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by amoto on 2/5/17.
 */
@RestController
public class UsersController {

    @RequestMapping("/app/user")
    public Principal user(Principal user) {
        return user;
    }
}
