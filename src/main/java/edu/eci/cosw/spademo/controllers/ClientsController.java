package edu.eci.cosw.spademo.controllers;

/**
 * Created by 2105684 on 2/8/17.
 */

import edu2.eci.cosw.stubs.fakeclientslibrary.CliendLoadException;
import edu2.eci.cosw.stubs.fakeclientslibrary.Client;
import edu2.eci.cosw.stubs.fakeclientslibrary.ClientServicesStub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/clients")
public class ClientsController {

    public ClientServicesStub clients= new ClientServicesStub();

    @RequestMapping(method = RequestMethod.GET)
    public Set<Client> getAllClients(){
        return clients.getAllClients();
    }

    @RequestMapping(value="/{id}" , method = RequestMethod.GET)
    public Client getClientById(@PathVariable Integer id){
        try {
            return clients.getClientById(id);
        } catch (CliendLoadException e) {
            return null;
        }
    }

    @RequestMapping(value="/{id}/picture" , method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> getClientPictureById(@PathVariable Integer id){
        try {
            return ResponseEntity.ok().contentType(MediaType.parseMediaType("image/jpg")).body(new InputStreamResource(clients.getClientPicture(id)));
        } catch (CliendLoadException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
