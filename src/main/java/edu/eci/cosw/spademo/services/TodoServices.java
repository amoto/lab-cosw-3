package edu.eci.cosw.spademo.services;

import edu.eci.cosw.spademo.models.Todo;

import java.util.ArrayList;

/**
 * Created by amoto on 2/5/17.
 */
public interface TodoServices {

    /**
     * returns all the registered todos
     * @return all the registered todos
     */
    public ArrayList<Todo> getTodos();

    /**
     * registers a todo
     * @param todo the todo to be registered
     */
    public void addTodo(Todo todo);

}
