'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl',  ['$scope', 'factory','Items', function($scope,factory,Items){
    $scope.todoNombre='';$scope.todoNumero=0;
    $scope.newTodo=function(){
        if($scope.todoNumero>=1 && $scope.todoNumero<10){
            /*factory.addTodo(
                {
                "description":$scope.todoNombre,"priority":$scope.todoNumero
                }
            );*/
            Items.save({"description":$scope.todoNombre,"priority":$scope.todoNumero})
        }
    }

}]);
