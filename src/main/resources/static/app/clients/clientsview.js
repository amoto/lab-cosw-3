'use strict';

angular.module('myApp.clients', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/clients', {
    templateUrl: 'clients/clientsview.html',
    controller: 'ViewClientsCtrl'
  });
}])

.controller('ViewClientsCtrl', ['$scope','$mdDialog','Clients',function($scope,$mdDialog,Clients) {
    //$scope.clientes=[{"id":1,"name":"Juan Perez","age":33,"profileDescription":"Este es mi perfil."},{"id":2,"name":"Pedro Rodrguez","age":33,"profileDescription":"Este es mi perfil, el de Pedro."},{"id":3,"name":"Maria Gonzales","age":28,"profileDescription":"Sin actualizar."},{"id":4,"name":"Juana Lopez","age":44,"profileDescription":"En construcciom."}];
    console.log(Clients);
    $scope.clientes=Clients.query();
    $scope.showAlert = function(ev,cli) {
        // Appending dialog to document.body to cover sidenav in docs app
        // Modal dialogs should fully cover application
        // to prevent interaction outside of dialog
        $mdDialog.show(
          $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(cli.name)
            .textContent(cli.profileDescription)
            .ok('Ok')
            .targetEvent(ev)
        );
      };

}]);