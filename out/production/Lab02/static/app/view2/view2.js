'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl',  ['$scope', 'factory', function($scope,factory){
    $scope.todoNombre='';$scope.todoNumero=0;
    $scope.newTodo=function(){
    console.log("asdasd "+$scope.todoNumero);
        if($scope.todoNumero>=1 && $scope.todoNumero<10){
            factory.addTodo(
                {
                "nombre":$scope.todoNombre,"numero":$scope.todoNumero
                }
            );
        }
    }

}]);
